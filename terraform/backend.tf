terraform {
  backend "azurerm" {
    resource_group_name  = "gitlab-resources"
    storage_account_name = "terraformclayassignment"
    container_name       = "tfstate"
    key                  = "terraform.tfstate"
    use_msi              = true
  }
}