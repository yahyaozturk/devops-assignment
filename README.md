# SALTO KS Devops Assignment

Here is the detailed problem definition

# Background
SaltoKS is moving to Gitlab to store and build our code.
We would like to fully automate the management of our Azure resources using terraform and Gitlab CI.

# Problem
We have a git repository holding our infrastructure code for one of our services. This is a very simple terraform code which creates a Resource Group and a Blob Storage.
The code is accessible at https://gitlab.com/claysolutions/infra/devops-assignment

# Tasks
- Create a design document explaining the way you'll execute the terraform code.
- Fork the repo above and make sure you can run the 'terraform apply' to create those resources in above-mentioned repo in a newly created Azure subscription (you need to signup to Azure for this). Feel free to modify the .gitlab-ci.yml file if necessary.
- Create a git repository and add any code you had to write to be able to execute the build.

There is one very important constraint: We don't trust in gitlab.com to store our credentials. So you will have to figure out a way to create a GitLab runner and authenticate it with azure in order to automate your terraform provisioning.
Do not hesitate to email if there are questions regarding this assignment

# Proposed Solution
## Problems
This assignment has several problems to be solved
- Gitlab is not a trusted environment, credentials can not be stored on it either in the repo or runtime variable
- Terraform state stored on local, this should be generally available because of infra history
- Gitlab Runner should be provisioned automatically and needs to be ready for build without manual interaction
- Gitlab Runner needs to be unregistered during de-provisioning
- terraform scripts needs to be modified in order to be able to execute the build

## Solution
In order to secure CI/CD process I used Managed Service Identity method (https://www.terraform.io/docs/providers/azurerm/guides/managed_service_identity.html).

Managed identities for Azure resources can be used to authenticate to services that support Azure Active Directory (Azure AD) authentication. There are two types of managed identities: system-assigned and user-assigned. This article is based on system-assigned managed identities.

For more information please visit : https://docs.microsoft.com/en-us/azure/active-directory/managed-identities-azure-resources/overview

Completed solution document here https://docs.google.com/document/d/1RHlFFhR6SHm6FTO9KAlfxZ4YpVhr0ZHtGMWQ-DC-P08/

## How to apply the solution
Please change variables to apply terraform script, below instruction will provision 
- make sure to have azure cli installed
- make sure to have terraform installed
- make sure to have azure subscription with required permission

```
clone the repo
cd terraform/gitlab-runner
az login
terraform init
terraform plan
terraform apply /
-var="location=northeurope" /
-var="prefix=gitlab" /
-var="environment=Gitlab" /
-var="admin_username=yahyaozturk" /
-var="admin_password=<password>" /
-var="gitlab_server=https://gitlab.com/" /
-var="gitlab_token=<token>" 
```

Once you run the script it will provision gitlab-runner, then you are ready to run Gitlab pipeline to provision given terraform script

